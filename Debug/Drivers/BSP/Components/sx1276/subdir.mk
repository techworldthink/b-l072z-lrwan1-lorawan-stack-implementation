################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BSP/Components/sx1276/sx1276.c 

OBJS += \
./Drivers/BSP/Components/sx1276/sx1276.o 

C_DEPS += \
./Drivers/BSP/Components/sx1276/sx1276.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/Components/sx1276/%.o Drivers/BSP/Components/sx1276/%.su Drivers/BSP/Components/sx1276/%.cyclo: ../Drivers/BSP/Components/sx1276/%.c Drivers/BSP/Components/sx1276/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/App/Core/Inc" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/App/LoRaWAN/inc" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/CMWX1ZZABZ-0xx" -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Crypto" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac/region" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Patterns/Basic" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Phy" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Utilities" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/B-L072Z-LRWAN1" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/Components/sx1276" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-BSP-2f-Components-2f-sx1276

clean-Drivers-2f-BSP-2f-Components-2f-sx1276:
	-$(RM) ./Drivers/BSP/Components/sx1276/sx1276.cyclo ./Drivers/BSP/Components/sx1276/sx1276.d ./Drivers/BSP/Components/sx1276/sx1276.o ./Drivers/BSP/Components/sx1276/sx1276.su

.PHONY: clean-Drivers-2f-BSP-2f-Components-2f-sx1276

