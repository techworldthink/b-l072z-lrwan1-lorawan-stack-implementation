################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../App/Core/Src/mlm32l0xx_hal_msp.c \
../App/Core/Src/mlm32l0xx_hw.c \
../App/Core/Src/mlm32l0xx_it.c 

OBJS += \
./App/Core/Src/mlm32l0xx_hal_msp.o \
./App/Core/Src/mlm32l0xx_hw.o \
./App/Core/Src/mlm32l0xx_it.o 

C_DEPS += \
./App/Core/Src/mlm32l0xx_hal_msp.d \
./App/Core/Src/mlm32l0xx_hw.d \
./App/Core/Src/mlm32l0xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
App/Core/Src/%.o App/Core/Src/%.su App/Core/Src/%.cyclo: ../App/Core/Src/%.c App/Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/App/Core/Inc" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/App/LoRaWAN/inc" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/CMWX1ZZABZ-0xx" -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Crypto" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Mac/region" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Patterns/Basic" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Phy" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/Middlewares/LoRaWAN/Utilities" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/B-L072Z-LRWAN1" -I"C:/Users/jobin/Documents/STM32/_B-L072z-lrwan1_generic_lorawan-master/_B-L072z-lrwan1_generic_lorawan-master/Drivers/BSP/Components/sx1276" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-App-2f-Core-2f-Src

clean-App-2f-Core-2f-Src:
	-$(RM) ./App/Core/Src/mlm32l0xx_hal_msp.cyclo ./App/Core/Src/mlm32l0xx_hal_msp.d ./App/Core/Src/mlm32l0xx_hal_msp.o ./App/Core/Src/mlm32l0xx_hal_msp.su ./App/Core/Src/mlm32l0xx_hw.cyclo ./App/Core/Src/mlm32l0xx_hw.d ./App/Core/Src/mlm32l0xx_hw.o ./App/Core/Src/mlm32l0xx_hw.su ./App/Core/Src/mlm32l0xx_it.cyclo ./App/Core/Src/mlm32l0xx_it.d ./App/Core/Src/mlm32l0xx_it.o ./App/Core/Src/mlm32l0xx_it.su

.PHONY: clean-App-2f-Core-2f-Src

