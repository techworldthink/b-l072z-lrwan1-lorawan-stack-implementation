# B-L072Z-LRWAN1 LoRaWAN Stack Implementation

## Change these values to use OTA activation of the end device.

### OVER_THE_AIR_ACTIVATION  1
### LORAWAN_DEVICE_EUI -> Device EUI from Chirpstack
### LORAWAN_JOIN_EUI -> Join EUI from Chirpstack
### LORAWAN_NWK_KEY -> Application key from Chirpstack